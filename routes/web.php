<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    
    $payload = [
        
        [
            'user_id' => 2,
            'first_name' => 'John',
            'last_name' => 'Doe'
        ],
    
        
        [
            'product_id' => 732,
            'description' => 'Product Description',
            'amount' => 19.99
        ],
        
    ];
    
    //fire('events', $payload);
    //fire('event', $payload);
    fire('events', $payload);
    Log::info('Event sent to rabbitmq');

    return abort(401);
});
