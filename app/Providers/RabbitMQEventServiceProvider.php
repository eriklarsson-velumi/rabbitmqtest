<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Nuwber\Events\BroadcastEventServiceProvider;

class RabbitMQEventServiceProvider extends BroadcastEventServiceProvider
{
    protected $listen = [
        'events' => [
            'App\Listeners\EventLogger',
        ],
    ];
    
 
}
