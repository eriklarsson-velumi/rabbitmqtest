<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Service;
use App\Applications\Application;

use Uuid;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $app_id = $request->app_id;
        
        $application = Application::findOrFail($app_id);
        $collection = collect($application->meta);

        $keyed = $collection->mapWithKeys(function ($item) {
            return [$item['meta_name'] => $item['meta_value']];
        });
        



        $service = new Service;
        $service->id = Uuid::generate();
        $service->environment = 'stage';
        $service->image = 'velumi/wordpress';
        $service->image_version = "1.6";
        $service->price = 0;
        $service->app_id = "f3f80040-35e1-11e9-8fdf-d99a582a9f49";
        $service->service_type = 'web';
        $service->save();
        

    }

  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
