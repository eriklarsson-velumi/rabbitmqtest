<?php

namespace App\Applications;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    //

    public function meta() {
        return $this->hasMany('App\Applications\ApplicationMetaData');
    }
}
