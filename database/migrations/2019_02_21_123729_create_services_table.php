<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            //
            $table->uuid('id');
            $table->string('environment', 45);
            $table->string('image', 45);
            $table->string('state', 45)->default('NEW');
            $table->string('image_version',10);
            $table->string('service_type', 10);
            $table->bigInteger('price')->default(0);
            $table->uuid('application_id');
            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services');
    }
}
